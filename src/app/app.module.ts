import { RouterModule, Routes } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//servicios
import { EquipoService } from './equipo.service';

import { AppComponent } from './app.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { ContactoComponent } from './contacto/contacto.component';
import { FooterComponent } from './footer/footer.component';
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ErrorComponent } from './error/error.component';
import { EquipoComponent } from './equipo/equipo.component';

const routes: Routes = [
  { path : 'contacto', component: ContactoComponent},
  { path : 'equipo/:id', component: EquipoComponent},
  { path : 'nosotros', component: NosotrosComponent},
  { path : 'error', component: ErrorComponent},
  { path : '', component: InicioComponent, pathMatch:'full'},
  { path : '**', redirectTo: 'error', pathMatch:'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    ContactoComponent,
    FooterComponent,
    InicioComponent,
    NosotrosComponent,
    ErrorComponent,
    EquipoComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    EquipoService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
