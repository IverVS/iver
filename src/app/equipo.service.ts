import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EquipoService 
{
  equipo:any[]=[
    {
      nombre:' Iver',
      especialidad:' HTML',
      descripcion:' Muy bien'
    },
    {
      nombre:' Nelly',
      especialidad:' CSS',
      descripcion:' Muy buena' 
    }
  ]
  constructor()
   {
     console.log('funcionando servicio')
    }

    obtenerEquipo()
    {
      return this.equipo;
    }

    obtenerUno(i)
    {
      return this.equipo[i];
    }
}
